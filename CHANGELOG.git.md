# Changelog

All changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.2.1] - 2024-01-09 

### Bug Fixes

- **3fb5327** - regression where the commits for the first tag are not shown

### Chores

- **e0f9b33** - bump version to 0.2.1
- **baae140** - update git changelog
- **273ac58** - update git changelog

## [v0.2.0] - 2024-01-09 

### Features

- **6a0ff24** - sort tags by semver
- **e1d8940** - impl support for user provided path to config file
- **67404f2** - add support for mapping commit scope to title
- **fca572a** - impl support for disabling loading config files
- **0491915** - support loading args from config file
- **a5a2fea** - support user provided templates
- **2f5ae29** - support rendering tera output to file
- **307cea5** - support rendering to tera template
- **1f1481e** - add serialization support

### Bug Fixes

- **f5d0d17** - make repo arg optional
- **5c91ea3** - apply fixes from clippy lints
- **2102711** - get commits where tag is at the HEAD and commits from the first tag

### Code Refactoring

- **eb2e57c** - rename context key `repo_remote` to `remote`

### Chores

- **902a02b** - bump version to 0.2.0
- **e3f812b** - update changelogs
- **5612a7f** - update git changelog
- **533ecfa** - add info to manifest files

### Documentation

- **73184db** - update template
- **7781006** - add changelog
- **af91f3e** - update readme in preparation for v0.2.0 release
- **8dcf9be** - update readme with installation instructions
- **c36524e** - update changelog
- **d2f25fc** - update readme
- **c715d99** - update changelog
- **b284eea** - update readme with usage and examples
- **e822048** - update readme with documentation on templates
- **4a84029** - update changelog to use new title mapping feature
- **cc5841d** - update README
- **3f06f78** - add README
- **04f46d2** - add git changelog

## [v0.1.0] - 2024-01-07 

### Chores

- **658054e** - bump version to 0.1.0

### Features

- **9c4bb8c** - add cli to specify repo to use
- **1909f75** - group commits by tag and refactor structs into thier own modules
- **c43c47c** - group commits by scope

### Other

- **06bc328** - Initial commit

[Unreleased]: https://gitlab.com/Th3-Wr41th/changelog/compare/v0.2.1...HEAD
[v0.2.1]: https://gitlab.com/Th3-Wr41th/changelog/compare/v0.2.0...v0.2.1
[v0.2.0]: https://gitlab.com/Th3-Wr41th/changelog/compare/v0.1.0...v0.2.0
[v0.1.0]: https://gitlab.com/Th3-Wr41th/changelog/commits/v0.1.0
