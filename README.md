# Changelog

Generate changelogs from git history, useful as a reference point for writing changelogs, groups commits by scope an its parent tag.

## Install

Changelog is not available on `crates.io` at this time, so has to be installed from git as follows:

```sh
$ cargo install --git https://gitlab.com/Th3-Wr41th/changelog
```

To use a specific version use the `--tag` flag followed the version to use:

```sh
$ cargo install --git https://gitlab.com/Th3-Wr41th/changelog --tag v0.2.0
```

## Usage

`changelog` requires a template to generate a changelog. An example can be found in the [templates](templates) directory, and details on the keys available can be found in the [templates](#templates) section.

To generate a changelog simply provide the path to the template with the `-t | --template` flag and the path to the repo to generate a changelog for, as follows:

```sh
$ changelog --template ./path/to/template.md .
```

To output the generated changelog to a file, provide the `-o | --output` flag followed by the path to the file, as follows:

```sh
$ changelog -t ./path/to/template.md --output CHANGELOG.git.md .
```

If you have a remote repository, for example on gitlab or github, and want to provide a link to it in the template, for example in comparing tags, you can provide the `-r | --remote` flag followed by a string containing the url, as follows:

```sh
$ changelog -t ./path/to/template.md --remote "https://gitlab.com/user/repo" .
```

## Configuration

It may be useful to use a config file instead of arguments, `changelog` looks for config files in two places `.changelog/config.toml` or `~/.config/changelog/config.toml`. It is recommend to use the first option as can be tracked by git as shared between contributers, if you wish to use a user level config please note template should be an absolute path. All paths are relative to the current working directory where the command was executed.

Config files use the same keys as the command line arguments but adds an additional table `title_map` for mapping scopes to titles, for example map `feat` to `Features`, as shown in the example below:

```toml
repo = "."
remote = "https://gitlab.com/user/repo"
output = "CHANGELOG.git.md"
template = "./path/to/template.md"

[title_map]
feat = "Features"
fix = "Bug Fixes"
perf = "Performance Improvements"
refactor = "Code Refactoring"
docs = "Documentation"
ci = "CI/CD"
chore = "Chores"
```

## Templates

Changelog uses [Tera](https://keats.github.io/tera) for templating, for all features in templates look at the [Tera Templates Documentation](https://keats.github.io/tera/docs/#templates).

Changelog exposes 2 keys to templates `tags` and `remote`. `remote` is an optional string, typically pointing to remote repository, gitlab or github etc. `tags` is an array of serialized data structures with the following fields:

```
tags: [
  Tag {
    name,
    date,
    commit_groups: [
      CommitGroup {
        scope,
        commits: [
          Commit {
            id,
            summary,
            message,
          }
        ]
      }
    ],
    target_id,
    prev_tag,
  }
]
```

## Notes

`CHANGELOG.git.md` is built with this tool

Based on [`git-chglog`](https://github.com/git-chglog/git-chglog)

## License

This project is licensed under the [Mozilla Public License 2.0](LICENSE)
