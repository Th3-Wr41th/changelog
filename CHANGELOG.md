# Changelog

All changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.2.1] - 2024-01-09

### Fixed

- Regression where commits for the first tag are not shown 

## [v0.2.0] - 2024-01-09

### Added

- Support for config files both by looking is specified directory or user provided, or can be disabled outright
- Support for mapping scopes to titles, ie `feat` to `Features`
- Support for writing output using user provided templates to either stdout or user specified file
- Sort tags by their semantic version

### Changed

- Made the `repo` argument optional
- Renamed context key `repo_remote` to `remote`

### Fixed

- Bug where commits would not be shown if the HEAD was the same place as a tag or it is the first tag

## [v0.1.0] - 2024-01-07

### Added

- Command line interface to specify a repo to generate a changelog for
- Support for grouping commit by their tag and scope

[Unreleased]: https://gitlab.com/Th3-Wr41th/changelog/compare/v0.2.0...HEAD
[v0.2.0]: https://gitlab.com/Th3-Wr41th/changelog/compare/v0.1.0...v0.2.0
[v0.1.0]: https://gitlab.com/Th3-Wr41th/changelog/commits/v0.1.0
