// SPDX-License-Identifier: MPL-2.0

use std::sync::Arc;

use clap::Parser;

/// Generate a changelog from git commit history
#[derive(Debug, Parser, Clone)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Path to repo
    pub repo: Option<Arc<str>>,

    /// URL of remote repository
    #[arg(short, long)]
    pub remote: Option<Arc<str>>,

    /// File to write output to
    #[arg(short, long)]
    pub output: Option<Arc<str>>,

    /// Path to a template to use
    #[arg(short, long)]
    pub template: Option<Arc<str>>,

    /// Use alternate config file
    #[arg(long)]
    pub config: Option<Arc<str>>,

    /// Do not load any config file
    #[arg(long)]
    pub no_config: bool,
}
