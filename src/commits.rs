// SPDX-License-Identifier: MPL-2.0

use std::{collections::HashMap, rc::Rc};

use git2::{Repository, Revwalk};
use serde::Serialize;

use crate::{revwalk_sort, TitleMap};

#[derive(Debug, Clone, Serialize)]
pub struct CommitGroup {
    pub scope: Rc<str>,
    pub commits: Vec<Commit>,
}

#[derive(Debug, Clone, Serialize)]
pub struct Commit {
    pub id: Rc<str>,
    pub summary: Rc<str>,
    pub message: Rc<str>,
}

impl CommitGroup {
    pub fn from_revwalk(repo: &Repository, revwalk: Revwalk, title_map: &TitleMap) -> anyhow::Result<Vec<Self>> {
        let commits: Vec<_> = revwalk
            .map(|id| {
                let id = id?;
                let commit = repo.find_commit(id)?;

                let short_id = {
                    let mut id = commit.id().to_string();

                    id.truncate(7);

                    id
                };

                let Some(summary) = commit.summary() else {
                    bail!("Summary is not valid");
                };

                let message = {
                    if let Some(message) = commit.message() {
                        let mut lines = message.lines();

                        // First line is the summary
                        let _ = lines.next();

                        let lines = lines.filter(|line| !line.is_empty()).collect::<Vec<&str>>();

                        lines.join("\n")
                    } else {
                        bail!("Message is not valid");
                    }
                };

                let (scope, summary) = summary
                    .split_once(':')
                    .map_or(("Other", summary.trim()), |(scope, summary)| {
                        (scope.trim(), summary.trim())
                    });

                let scope = match title_map.get(scope) {
                    Some(val) => val,
                    None => scope
                };

                let commit = Commit {
                    id: short_id.into(),
                    summary: summary.into(),
                    message: message.into(),
                };

                Ok::<(Rc<str>, Commit), anyhow::Error>((scope.into(), commit))
            })
            .collect::<anyhow::Result<_>>()?;

        let mut res: HashMap<Rc<str>, Vec<Commit>> = HashMap::new();

        for (scope, commit) in &commits {
            if let Some(vec) = res.get_mut(scope) {
                vec.push(commit.clone());
            } else {
                let vec: Vec<Commit> = vec![commit.clone()];

                res.insert(scope.clone(), vec);
            }
        }

        let commit_groups: Vec<CommitGroup> = res
            .iter()
            .map(|(scope, commits)| CommitGroup {
                scope: scope.clone(),
                commits: commits.clone(),
            })
            .collect();

        Ok(commit_groups)
    }

    pub fn from_range(repo: &Repository, range: &str, title_map: &TitleMap) -> anyhow::Result<Vec<Self>> {
        let mut revwalk = repo.revwalk()?;

        revwalk.set_sorting(revwalk_sort!())?;

        revwalk.push_range(range)?;

        CommitGroup::from_revwalk(repo, revwalk, title_map)
    }
}
