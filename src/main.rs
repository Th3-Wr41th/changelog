// SPDX-License-Identifier: MPL-2.0

use anyhow::Context;
use clap::Parser;
use config::Config;
use git2::{Oid, Repository};
use std::{collections::HashMap, fs::File, path::Path, sync::Arc};
use tera::Tera;

#[macro_use]
extern crate anyhow;

mod cli;
mod commits;
mod config;
mod tags;

use commits::CommitGroup;
use tags::Tag;

#[macro_export]
macro_rules! revwalk_sort {
    () => {
        git2::Sort::TIME & git2::Sort::TOPOLOGICAL & git2::Sort::REVERSE
    };
}

type TitleMap = HashMap<Arc<str>, Arc<str>>;

fn get_from_repo(repo: &Repository, title_map: TitleMap) -> anyhow::Result<Vec<Tag>> {
    let mut tags: Vec<Tag> = Tag::get_all(repo)?;

    if tags.is_empty() {
        let mut revwalk = repo.revwalk()?;

        revwalk.set_sorting(revwalk_sort!())?;

        revwalk.push_head()?;

        let commit_groups: Vec<CommitGroup> = CommitGroup::from_revwalk(repo, revwalk, &title_map)?;

        let head = repo.head()?;

        let target_id = head.target().unwrap_or(Oid::zero());

        let tag = Tag {
            name: "Unreleased".into(),
            date: "".into(),
            commit_groups,
            target_id: target_id.to_string().into(),
            prev_tag: None,
        };

        tags = vec![tag];
    } else {
        let range = format!("{}..HEAD", tags[0].target_id);

        let unreleased: Vec<CommitGroup> = CommitGroup::from_range(repo, &range, &title_map)?;

        let tag = Tag {
            name: "Unreleased".into(),
            date: "".into(),
            commit_groups: unreleased,
            target_id: "HEAD".into(),
            prev_tag: Some(tags[0].name.clone()),
        };

        tags.insert(0, tag);

        let tags_copy = tags.clone();
        let mut iter = tags_copy.iter().enumerate();

        let _ = iter.next();

        let maxidx = tags_copy.len() - 1;

        while let Some((idx, tag)) = iter.next() {
            if idx == maxidx {
                let mut revwalk = repo.revwalk()?;

                revwalk.set_sorting(revwalk_sort!())?;

                let oid = Oid::from_str(&tag.target_id)?;

                revwalk.push(oid)?;

                let group = CommitGroup::from_revwalk(repo, revwalk, &title_map)?;

                tags[idx].commit_groups = group;
            } else {
                let range = format!("{}..{}", tags[idx + 1].target_id, tag.target_id);
                let group = CommitGroup::from_range(repo, &range, &title_map)?;

                tags[idx].commit_groups = group;
                tags[idx].prev_tag = Some(tags[idx + 1].name.clone());
            }
        }
    }

    Ok(tags)
}

fn init_tera<P: AsRef<Path>>(template: P) -> tera::Result<Tera> {
    let mut tera = Tera::default();

    tera.add_template_file(template, Some("tpl.md"))?;

    Ok(tera)
}

fn main() -> anyhow::Result<()> {
    let args = cli::Args::parse();

    let args = if !args.no_config {
        match Config::load(&args.config) {
            Ok(config) => config.with_args(&args),
            Err(err) => {
                println!("Warning: {err}");

                Config::default().with_args(&args)
            }
        }
    } else {
        Config::default().with_args(&args)
    };

    let Some(repo) = args.repo else {
        bail!("no repository provided");
    };

    let Some(template) = args.template else {
        bail!("no template provided");
    };

    let tera = init_tera(template.as_ref()).context("failed to inititalize tera")?;

    let repo = Repository::open(repo.as_ref()).context("failed to open repository")?;

    let data = get_from_repo(&repo, args.title_map)?;

    let mut context = tera::Context::default();

    context
        .try_insert("remote", &args.remote)
        .context("failed to serialize remote")?;
    context
        .try_insert("tags", &data)
        .context("failed to serialize data")?;

    if let Some(path) = args.output {
        let file = File::create(path.as_ref()).context("failed to open file")?;

        tera.render_to("tpl.md", &context, file)
            .context("failed to render to file")?;
    } else {
        let rendered = tera
            .render("tpl.md", &context)
            .context("failed to render data to template")?;

        println!("{rendered}");
    }

    Ok(())
}
