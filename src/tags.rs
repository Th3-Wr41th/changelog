// SPDX-License-Identifier: MPL-2.0

use anyhow::Context;
use chrono::{DateTime, FixedOffset, LocalResult, TimeZone};
use git2::{Oid, Repository};
use semver::Version;
use serde::Serialize;
use std::rc::Rc;

use crate::commits::CommitGroup;

#[derive(Debug, Clone, Serialize)]
pub struct Tag {
    pub name: Rc<str>,
    pub date: Rc<str>,
    pub commit_groups: Vec<CommitGroup>,
    pub target_id: Rc<str>,
    pub prev_tag: Option<Rc<str>>,
}

impl Tag {
    pub fn get_all(repo: &Repository) -> anyhow::Result<Vec<Self>> {
        macro_rules! ok {
            (result $what:expr) => {
                match $what {
                    Ok(val) => val,
                    Err(err) => bail!(err),
                }
            };

            (option $what:expr) => {
                match $what {
                    Some(val) => val,
                    None => bail!("Something"),
                }
            };
        }

        let mut raw_tags: Vec<(Rc<str>, Oid)> = Vec::new();

        repo.tag_foreach(|tag: Oid, _| match repo.find_tag(tag) {
            Ok(tag) => {
                raw_tags.push((tag.name().unwrap_or("unknown").into(), tag.id()));
                true
            }
            Err(_err) => false,
        })
        .context("failed to iterate over tag")?;

        raw_tags.sort_by(|(a, _), (b, _)| {
            Version::parse(b.trim_start_matches('v'))
                .unwrap()
                .cmp_precedence(&Version::parse(a.trim_start_matches('v')).unwrap())
        });

        let tags: Vec<Tag> = raw_tags
            .iter()
            .map(|(_, tag)| {
                let tag = ok!(result repo.find_tag(*tag));

                let target = ok!(result tag.target());
                let target_id = target.id();

                if target_id.is_zero() {
                    bail!("tag has no target id");
                }

                let when = if let Some(tagger) = tag.tagger() {
                    let when = tagger.when();

                    let offset = ok!(option FixedOffset::east_opt(when.offset_minutes() * 60i32));

                    let datetime: DateTime<FixedOffset> =
                        match offset.timestamp_opt(when.seconds(), 0) {
                            LocalResult::Single(datetime) => datetime,
                            _ => bail!("Unable to format tag datetime"),
                        };

                    let date = format!("{}", datetime.format("%Y-%m-%d"));

                    date
                } else {
                    bail!("Unable to get who made this tag");
                };

                let name = ok!(option tag.name());

                Ok(Tag {
                    name: name.into(),
                    date: when.into(),
                    commit_groups: Vec::new(),
                    target_id: target_id.to_string().into(),
                    prev_tag: None,
                })
            })
            .collect::<anyhow::Result<Vec<Tag>>>()?;

        Ok(tags)
    }
}
