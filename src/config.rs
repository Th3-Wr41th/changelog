// SPDX-License-Identifier: MPL-2.0

use std::{collections::HashMap, fs, path::Path, sync::Arc};

use anyhow::Context;
use serde::Deserialize;

use crate::cli::Args;

#[derive(Debug, Default, Deserialize, Clone)]
pub struct Config {
    pub repo: Option<Arc<str>>,
    pub remote: Option<Arc<str>>,
    pub output: Option<Arc<str>>,
    pub template: Option<Arc<str>>,
    pub title_map: HashMap<Arc<str>, Arc<str>>,
}

impl Config {
    pub fn load(path: &Option<Arc<str>>) -> anyhow::Result<Self> {
        macro_rules! try_or_continue {
            ($what:expr) => {
                match $what {
                    Ok(val) => val,
                    Err(_err) => continue,
                }
            };
        }

        let mut config: Option<Self> = None;

        if let Some(path) = path {
            let content = fs::read_to_string(path.as_ref()).context("failed to read config file")?;

            let data: Self =
                toml::from_str(&content).context("failed to deserialize config data")?;

            config = Some(data);
        } else {
            let paths: [&str; 2] = [
                ".changelog/config.toml",
                &format!("{0}/.config/changelog/config.toml", env!("HOME")),
            ];

            let valid_paths: Vec<_> = paths
                .iter()
                .filter(|path| Path::new(path).exists())
                .collect();

            for path in valid_paths {
                let content = try_or_continue!(fs::read_to_string(path));

                let data = try_or_continue!(toml::from_str(&content));

                config = Some(data);

                break;
            }
        }

        if let Some(config) = config {
            Ok(config)
        } else {
            Err(anyhow!("No config file found"))
        }
    }

    pub fn with_args(&self, args: &Args) -> Self {
        Self {
            repo: args.repo.clone().or(self.repo.clone()),
            remote: args.remote.clone().or(self.remote.clone()),
            output: args.output.clone().or(self.output.clone()),
            template: args.template.clone().or(self.template.clone()),
            title_map: self.title_map.clone(),
        }
    }
}
